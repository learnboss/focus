# focus

template focus for learndash

**Visually impressive courses that learners will love and that give your brand a professional, polished image.**

Here’s a hard truth: you can have amazing course content but if its presentation is lack-luster then it doesn’t really matter. Online course plugins and hosted course offerings are lazy (and sometimes reckless) in this area.

Either the visual experience is too minimal with just plain-text, and therefore not contributing to an engaging learning experience.

Or, the templates are over-designed. Clashing with the rest of the website and becoming too distracting.

Both scenarios are lose-lose. Your students have a poor learning experience and your brand suffers for lack of professionalism. If you are selling courses then this could even lead to higher refund rates.

* Optimal course content display
* Intuitive and informative profiles
* Clear navigation tracking
* Multiple progress status points
* Clear success, information, and warning notifications
* Simple materials presentation
* We have taken care of these (and so much more) for you!


Literally every visual aspect in LD3 has an intentional, thoughtful design so that you and your learners both benefit.

***It is time for our expectations of online courses to evolve. The new LearnDash Focus Mode sets the bar higher.***


**Introducing LearnDash Focus Mode!**


<img src="https://learndashcom.lightningbasecdn.com/wp-content/uploads/learndash-course-focus-mode-animation-for-release-highres.gif"></img>


By simply activating a single setting you enable this distraction-free experience that looks great and is easy to use. LearnDash Focus Mode has been specifically designed to help increase both learner retention and course completion rates.

And given the ever increasing role that mobile learning has in online education, we took the time to make sure that your courses look just as good on mobile devices – giving your learners a consistent experience no matter how they decide to access your content.

Your learners can start the course on their laptop and pick-up where they left off on their phone. Your content will continue to shine because of the LearnDash Focus Mode delivery method.

But it’s more than just optimal content delivery, the user-experience of Focus Mode is on another level. Your learners can jump in-and-out of Focus Mode for an enjoyable and seamless course experience.


<img src="https://learndashcom.lightningbasecdn.com/wp-content/uploads/learndash-focus-mode-mobile-menu.gif"></img>


**Drag, Drop, and Move-on**


The clean interface of the LD3 course builder will have you spinning-up courses in minutes. If you have never created an online course before, then our course building experience will have you feeling like pro in no time!

<img src="https://learndashcom.lightningbasecdn.com/wp-content/uploads/learndash-course-builder-drag-drop-everything-animation.gif"></img>


**Reuse Your Content**


LearnDash is the only course builder that allows you to re-use your existing course content. No need to continually copy lessons or quizzes just to use them in other courses. Just select what you need and drag it over.

**The most advanced quizzing engine just got even better!**


Assessments play a core part in online courses. If your quizzing capabilities are limited then so too is your course. But what good are advanced quiz settings if you don’t know how to use them?

The LearnDash quiz capabilities are in a class of their own, but your feedback rightfully pointed out that using these features needed to be easier. The new LearnDash quiz builder is the result of this feedback!


**Drag & Drop Quiz Builder**


Once you use the course builder, you will know immediately how to use the quiz builder in LearnDash as the interface is exactly the same – significantly decreasing the learning curve!

<img src="https://learndashcom.lightningbasecdn.com/wp-content/uploads/learndash-quiz-builder-rearrange-questions-drag-drop.gif"></img>

**Create Questions Directly in the Builder**

You can now quickly add new questions and configure their settings directly in the builder! First you select your desired question type:



<img src="https://learndashcom.lightningbasecdn.com/wp-content/uploads/learndash-quiz-builder-edit-question-type-animation.gif"></img>

Once your question type is selected, the available settings will adjust and you can begin configuring the question!

<img src="https://learndashcom.lightningbasecdn.com/wp-content/uploads/learndash-quiz-builder-add-new-answer-animation.gif">